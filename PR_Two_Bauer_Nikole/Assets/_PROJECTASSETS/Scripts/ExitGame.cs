﻿using UnityEngine;

namespace PRTWO
{
    public class ExitGame : MonoBehaviour
    {
        /// <summary>
        /// Exits the Game-Application
        /// </summary>
        public void Exit()
        {
            Debug.Log("Test");
            Application.Quit();
        }
    }
}